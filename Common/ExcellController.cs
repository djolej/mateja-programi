﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Common
{
    public class ExcellController
    {
        private string DataSourcePath { get; set; } = null;
        private List<String> ColumnNames { get; set; }

        public ExcellController(string[] columnNames)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ColumnNames = new List<string>(columnNames);
        }

        public void SetPathIfNotSet()
        {
            if (DataSourcePath != null)
                return;
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    DataSourcePath = fbd.SelectedPath;
                }
            }
        }

        public void Export<T>(List<T> toExport, string fileName) where T : IExcelModel
        {
            while (DataSourcePath == null)
                SetPathIfNotSet();

            var fileWithPath = Path.Combine(DataSourcePath, fileName + ".xlsx");
            FileInfo file = new FileInfo(fileWithPath);

            using (ExcelPackage excel = new ExcelPackage(file))
            {
                // Initialization
                var sheet = excel.Workbook.Worksheets.Add("results");
                sheet.InsertColumn(1, ColumnNames.Count);

                int row = 1;
                sheet.InsertRow(row, 1);
                var displayRow = sheet.Row(1);
                int col = 1;
                foreach (var colName in ColumnNames)
                {
                    sheet.Cells[row, col++].Value = colName;
                }
                row++;

                foreach (var item in toExport)
                {
                    for (col = 1; col < ColumnNames.Count + 1; col++)
                    {
                        sheet.Cells[row, col, row, col].Value = item.GetValueForColumn(col);

                    }
                    row++;
                }

                excel.SaveAs(file);
            }
        }
    }
}
