﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Common
{
    public class StatusUpdatesUi
    {
        private RichTextBox _textBox;

        public StatusUpdatesUi(RichTextBox richTextBox)
        {
            _textBox = richTextBox;
        }

        public void LogMessage(string text)
        {
            AppendText(text, Color.Green);
        }

        public void LogError(string text)
        {
            AppendText(text, Color.Red);
        }

        private void AppendText(string text, Color color)
        {
            if (string.IsNullOrEmpty(text))
                return;

            if (_textBox.InvokeRequired)
            {
                _textBox.Invoke(new Action(() => { updateText(); }));
            }
            else
                updateText();

            void updateText()
            {
                _textBox.SelectionStart = _textBox.TextLength;
                _textBox.SelectionLength = 0;

                _textBox.SelectionColor = color;
                _textBox.AppendText(text);
                _textBox.SelectionColor = _textBox.ForeColor;

                _textBox.Text += Environment.NewLine;
                Application.DoEvents();
            }
        }
    }
}
