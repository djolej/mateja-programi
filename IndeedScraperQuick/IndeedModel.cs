﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IndeedScraperQuick
{
    public class IndeedModel : IExcelModel
    {
        public string Title { get; set; }

        public string Company { get; set; }

        public string Location { get; set; }

        public override string GetValueForColumn(int col)
        {
            switch (col)
            {
                case 1:
                    return Title;
                case 2:
                    return Company;
                case 3:
                    return Location;
                default:
                    return col.ToString();
            }
        }
    }
}
