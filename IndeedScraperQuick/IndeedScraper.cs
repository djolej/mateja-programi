﻿using Common;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IndeedScraperQuick
{
    public partial class IndeedScraper : Form
    {
        private StatusUpdatesUi _statusLogger;
        private List<IndeedModel> allElements = new List<IndeedModel>();
        private ExcellController _excellController;

        public IndeedScraper()
        {
            InitializeComponent();
            this.KeyPreview = true;
            _statusLogger = new StatusUpdatesUi(richTextBoxStatus);
        }

        private void buttonPaste_Click(object sender, EventArgs e)
        {
            var text = Clipboard.GetText(TextDataFormat.Html);
            _statusLogger.LogMessage("Pokupio HTML, analiziram");

            var elements = ExtractIndeedModelFromHtml(text);
            _statusLogger.LogMessage("Zavrsio sam sa pokupljanjem");
            _statusLogger.LogMessage("Ili klikni PASTE opet za sledecu stranu, ili klikni SACUVAJ da sacuvas u excell");
            allElements.AddRange(elements);
        }

        private List<IndeedModel> ExtractIndeedModelFromHtml(string html)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);

            var divsWithInfo = doc.DocumentNode.SelectNodes("//div[contains(@class, 'jobsearch-SerpJobCard')]");
            List<IndeedModel> toRet = new List<IndeedModel>();
            int pos = 0;
            foreach (var div in divsWithInfo)
            {
                try
                {
                    var res = GenerateModelFromDiv(div);
                    if (res != null)
                        toRet.Add(res);
                }
                catch (Exception e)
                {
                    _statusLogger.LogError($"Doslo je do greske na poziciji {pos}");
                    _statusLogger.LogError("Sacuvaj sta ti je u copy (url izvuci samo) i posalji djoletu sa sve pozicijom.");
                    _statusLogger.LogError("Prelazimo preko greske, idemo dalje");
                }
                pos++;
            }

            return toRet;
        }

        private IndeedModel GenerateModelFromDiv(HtmlNode div)
        {
            var titleDiv = div.SelectSingleNode(".//div[contains(@class, 'title')]");
            if (titleDiv == null)
                return null;
            string title = WebUtility.HtmlDecode(titleDiv.SelectSingleNode(".//a[contains(@class, 'jobtitle')]").InnerText);
            string companyName = WebUtility.HtmlDecode(div.SelectSingleNode(".//span[contains(@class, 'company')]").InnerText);
            string locationName = WebUtility.HtmlDecode(div.SelectSingleNode(".//span[contains(@class, 'location')]").InnerText);
            return new IndeedModel()
            {
                Title = title,
                Company = companyName,
                Location = locationName
            };
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            richTextBoxStatus.Clear();
            allElements.Clear();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (_excellController == null)
                _excellController = new ExcellController(new string[] { "Job title", "Kompanija", "Lokacija" });
            _excellController.Export(allElements, "indeedScraper-" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm"));
        }

        private void IndeedScraper_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                e.SuppressKeyPress = true;
                this.buttonPaste_Click(this, null);
            }
        }
    }
}
