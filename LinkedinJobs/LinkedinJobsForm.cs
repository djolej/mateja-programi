﻿using Common;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinkedinJobs
{
    public partial class LinkedinJobsForm : Form
    {
        private StatusUpdatesUi _statusLogger;
        private ExcellController _excellController;
        private List<JobsScrapeModel> allElements = new List<JobsScrapeModel>();

        public LinkedinJobsForm()
        {
            InitializeComponent();
            _statusLogger = new StatusUpdatesUi(richTextBoxStatus);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            allElements.Clear();
            richTextBoxStatus.Clear();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (_excellController == null)
                _excellController = new ExcellController(new string[] { "Job title", "Kompanija", "Lokacija" });
            _excellController.Export(allElements, "jobs-search-quick-" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm"));
            _statusLogger.LogMessage("ZAVRSIO");
        }

        private void buttonPaste_Click(object sender, EventArgs e)
        {
            var text = Clipboard.GetText(TextDataFormat.Html);
            _statusLogger.LogMessage("Pokupio HTML, analiziram");

            var elements = ExtractLinkedinItemsFromHtml(text);
            _statusLogger.LogMessage("Zavrsio sam sa pokupljanjem");
            _statusLogger.LogMessage("Ili klikni PASTE opet za sledecu stranu, ili klikni SACUVAJ da sacuvas u excell");
            allElements.AddRange(elements);
        }

        private List<JobsScrapeModel> ExtractLinkedinItemsFromHtml(string html)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            var divsWithInfo = doc.DocumentNode.SelectNodes("//div[contains(@class, 'job-card-search')]");
            List<JobsScrapeModel> toRet = new List<JobsScrapeModel>();
            int pos = 0;
            foreach (var div in divsWithInfo)
            {
                try
                {
                    var res = GenerateModelFromDiv(div);
                    if (res != null && res.JobAd != null)
                        toRet.Add(res);
                }
                catch (Exception e)
                {
                    _statusLogger.LogError($"Doslo je do greske na poziciji {pos}");
                    _statusLogger.LogError("Sacuvaj sta ti je u copy (url izvuci samo) i posalji djoletu sa sve pozicijom.");
                    _statusLogger.LogError("Prelazimo preko greske, idemo dalje");
                }
                pos++;
            }

            return toRet;
        }

        private JobsScrapeModel GenerateModelFromDiv(HtmlNode div)
        {
            string title = WebUtility.HtmlDecode(div.SelectSingleNode(".//h3[contains(@class, 'job-card-search__title')]")?.InnerText);
            string companyName = WebUtility.HtmlDecode(div.SelectSingleNode(".//a[contains(@class, 'job-card-search__company-name-link')]")?.InnerText);
            string locationName = WebUtility.HtmlDecode(div.SelectSingleNode(".//span[contains(@class, 'job-card-search__location')]")?.InnerText);
            return new JobsScrapeModel()
            {
                JobAd = title,
                Company = companyName,
                Location = locationName
            };
        }
    }
}
