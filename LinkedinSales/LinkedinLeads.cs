﻿using Common;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinkedinSales
{
    public partial class LinkedinLeads : Form
    {
        private StatusUpdatesUi _statusLogger;
        private ExcellController _excellController;
        private List<LinkedinLeadsModel> allElements = new List<LinkedinLeadsModel>();

        public LinkedinLeads()
        {
            InitializeComponent();
            _statusLogger = new StatusUpdatesUi(richTextBoxStatus);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            allElements.Clear();
            richTextBoxStatus.Clear();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (_excellController == null)
                _excellController = new ExcellController(new string[] { "Ime", "Prezime", "Job title", "Company", "Lokacija" });
            _excellController.Export(allElements, "leads-search-quick-" + DateTime.Now.ToString("yyyy-MM-dd_HH_mm"));
            _statusLogger.LogMessage("ZAVRSIO");
        }

        private void buttonPaste_Click(object sender, EventArgs e)
        {
            var text = Clipboard.GetText(TextDataFormat.Html);
            _statusLogger.LogMessage("Pokupio HTML, analiziram");

            var elements = ExtractLinkedinItemsFromHtml(text);
            _statusLogger.LogMessage("Zavrsio sam sa pokupljanjem");
            _statusLogger.LogMessage("Ili klikni PASTE opet za sledecu stranu, ili klikni SACUVAJ da sacuvas u excell");
            allElements.AddRange(elements);
        }

        private List<LinkedinLeadsModel> ExtractLinkedinItemsFromHtml(string html)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(html);
            var divsWithInfo = doc.DocumentNode.SelectNodes("//div[contains(@class, 'search-results__result-container')]");
            List<LinkedinLeadsModel> toRet = new List<LinkedinLeadsModel>();
            int pos = 0;
            foreach (var div in divsWithInfo)
            {
                try
                {
                    var res = GenerateModelFromDiv(div);
                    if (res != null && res.Name != null)
                        toRet.Add(res);
                }
                catch (Exception e)
                {
                    _statusLogger.LogError($"Doslo je do greske na poziciji {pos}");
                    _statusLogger.LogError("Sacuvaj sta ti je u copy (url izvuci samo) i posalji djoletu sa sve pozicijom.");
                    _statusLogger.LogError("Prelazimo preko greske, idemo dalje");
                }
                pos++;
            }

            return toRet;
        }

        private LinkedinLeadsModel GenerateModelFromDiv(HtmlNode div)
        {
            var section = div.SelectSingleNode(".//section[contains(@class, 'result-lockup')]");
            if (section == null) //javi err gore
                return null;

            var jobAndCompany = section.SelectSingleNode(".//dd[contains(@class, 'result-lockup__highlight-keyword')]");
            string jobTitle = string.Empty;
            string companyName = string.Empty;

            string nameSurname = WebUtility.HtmlDecode(section.SelectSingleNode(".//dt[contains(@class, 'result-lockup__name')]")?.InnerText);

            if (jobAndCompany != null)
            {
                jobTitle = WebUtility.HtmlDecode(jobAndCompany.FirstChild?.InnerText);
                    companyName = WebUtility.HtmlDecode(jobAndCompany.LastChild?.InnerText);
                if (companyName.StartsWith("at"))
                    companyName = companyName.Substring(2).Trim();
                companyName = companyName.Split(new string[] { "Go to " }, StringSplitOptions.None)[0].Trim();
            }
            string locationName = WebUtility.HtmlDecode(section.SelectSingleNode(".//li[contains(@class, 'result-lockup__misc-item')]")?.InnerText);
            var res = new LinkedinLeadsModel()
            {
                JobTitle = jobTitle,
                Company = companyName,
                Location = locationName
            };
            res.SolveNameSurname(nameSurname);

            return res;
        }
    }
}
