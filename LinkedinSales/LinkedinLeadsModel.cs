﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedinSales
{
    public class LinkedinLeadsModel : IExcelModel
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string JobTitle { get; set; }

        public string Company { get; set; }

        public string Location { get; set; }

        public override string GetValueForColumn(int col)
        {
            switch (col)
            {
                case 1:
                    return Name;
                case 2:
                    return Surname;
                case 3:
                    return JobTitle;
                case 4:
                    return Company;
                case 5:
                    return Location;
                default:
                    return col.ToString();
            }
        }

        public void SolveNameSurname(string nameSurnameStr)
        {
            var nameSurnameArr = nameSurnameStr.Split(' ');
            Name = nameSurnameArr[0];
            Surname = string.Join(" ", nameSurnameArr.Skip(1));
        }
    }
}
